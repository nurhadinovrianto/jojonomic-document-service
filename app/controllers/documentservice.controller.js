const db = require("../models");
const documentservice = db.documentservice;

 
  exports.create = (req, res) => {
  
    // Save Tutorial in the database
    documentservice.create({
    id_documentservice: req.body.id_documentservice,
      name: req.body.name,
      type:req.body.type,
      owner_id: req.body.owner_id,
      share: req.body.share,
      timestamp: req.body.timestamp
      })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the Tutorial."
          });
        });
  };
  
  exports.findOne = (req, res) => {
    const id = req.params.id;
    documentservice.findOne({
        where: {id:id}
    })
    .then(data =>{
        res.send(data);
    })

    .catch(err => {
        res.status(500).send({
          message: "Error retrieving Tutorial with id=" + id
        });
      });
  };
  
  exports.delete = (req, res) => {
      documentservice.destroy({
          where: {id: id}
      })

    .then(num => {
        if (num == 1) {
          res.send({
            message: "Tutorial was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Tutorial with id=" + id
        });
      });
  };