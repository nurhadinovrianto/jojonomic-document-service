module.exports = (sequelize, Sequelize) => {
    const documentservice = sequelize.define("documentservice", {
      id_documentservice: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      owner_id: {
        type: Sequelize.STRING
      },
      share: {
        type: Sequelize.STRING
      },
      timestamp: {
        type: Sequelize.STRING
      },
      company_id: {
        type: Sequelize.STRING
      }
    });
  
    return documentservice;
  };