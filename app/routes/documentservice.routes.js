const { authJwt } = require("../middleware");
const controller = require("../controllers/documentservice.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

 
  app.post(
    "/document-service/folder",
    [authJwt.verifyToken],
    controller.create
  );

  app.get(
    "/document-service/document/:document_id",
    [authJwt.verifyToken],
    controller.findOne
  );

  app.delete(
    "/document-service/document/:document_id",
    [authJwt.verifyToken],
    controller.delete
  );

};